export class User{
    firstName: string;
    lastName: string;
    userName: string;
    role: string;
}